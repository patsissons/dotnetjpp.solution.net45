﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DigitalHarmoniX.DotNETJPP.Test
{
    [TestClass]
    public class ParserTest
    {
        [TestMethod]
        public void StandardHashCommentTest()
        {
            const string input = @"
# Hash Comment
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(0, map.Count, "Unexpected number of keys");
        }

        [TestMethod]
        public void StandardBangCommentTest()
        {
            const string input = @"
! Bang Comment
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(0, map.Count, "Unexpected number of keys");
        }

        [TestMethod]
        public void SpacePrefixedHashCommentTest()
        {
            const string input = @"
   # Hash Comment
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(0, map.Count, "Unexpected number of keys");
        }

        [TestMethod]
        public void SpacePrefixedBangCommentTest()
        {
            const string input = @"
  ! Bang Comment
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(0, map.Count, "Unexpected number of keys");
        }

        [TestMethod]
        public void AllCommentsTest()
        {
            const string input = @"
# Hash Comment
! Bang Comment
   # Hash Comment
  ! Bang Comment
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(0, map.Count, "Unexpected number of keys");
        }

        [TestMethod]
        public void StandardKeyValuePairTest()
        {
            const string input = @"
key = value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void ColonKeyValuePairTest()
        {
            const string input = @"
key : value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void SpaceKeyValuePairTest()
        {
            const string input = @"
key value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void SpacePrefixedKeyValuePairTest()
        {
            const string input = @"
  key = value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void SpaceSuffixedKeyValuePairTest()
        {
            const string input = @"
key = value      
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void NoSpaceStandardKeyValuePairTest()
        {
            const string input = @"
key=value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void NoSpaceColonKeyValuePairTest()
        {
            const string input = @"
key:value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void NoSpaceSpaceKeyValuePairTest()
        {
            const string input = @"
key value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void StandardEmptyValueTest()
        {
            const string input = @"
key = 
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(string.Empty, map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void ColonEmptyValueTest()
        {
            const string input = @"
key : 
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(string.Empty, map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void SpaceEmptyValueTest()
        {
            const string input = @"
key 
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(string.Empty, map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void NoSpaceStandardEmptyValueTest()
        {
            const string input = @"
key=
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(string.Empty, map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void NoSpaceColonEmptyValueTest()
        {
            const string input = @"
key:
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(string.Empty, map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void NoSpaceSpaceEmptyValueTest()
        {
            // note that we need at least one space in there to act as the key value separator
            // but no other spaces are used to confirm this test
            const string input = @"
key 
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(string.Empty, map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void StandardEscapedKeyTest()
        {
            const string input = @"
key\:is\ escaped\=\\1 = value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key:is escaped=\1", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void StandardEscapedValueTest()
        {
            const string input = @"
key = value\ has\: escaped\ characters\: \ ,\:,\=
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value has: escaped characters:  ,:,=", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void NonStandardEscapedKeyTest()
        {
            const string input = @"
key\:\@\%\~\\ = value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key:@%~\", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void NonStandardEscapedValueTest()
        {
            const string input = @"
key = value\:\@\%\~\\.
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value:@%~\.", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void EscapedColonKeyTest()
        {
            const string input = @"
escaped\:key : value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"escaped:key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void EscapedSpaceKeyTest()
        {
            const string input = @"
escaped\ key value
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"escaped key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual(@"value", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void MultiLineValueTest()
        {
            const string input = @"
key = value \
      with \
multiple\
lines
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(1, map.Count, "Unexpected number of keys");
            Assert.AreEqual(@"key", map.Keys.Single(), "Unexpected Key");
            Assert.AreEqual("value \r\n      with \r\nmultiple\r\nlines", map.Values.Single(), "Unexpected Value");
        }

        [TestMethod]
        public void SampleFileTest()
        {
            // taken from http://en.wikipedia.org/wiki/.properties#Format
            const string input = @"
# You are reading the "".properties"" entry.
! The exclamation mark can also mark text as comments.
# The key and element characters #, !, =, and : are written with a preceding backslash to ensure that they are properly loaded.
website = http\://en.wikipedia.org/
language = English
# The backslash below tells the application to continue reading
# the value onto the next line.
message = Welcome to \
          Wikipedia!
# Add spaces to the key
key\ with\ spaces = This is the value that could be looked up with the key ""key with spaces"".
# Unicode
tab : \u0009
";

            var map = Parser.ParseText(input);

            Assert.IsNotNull(map, "Error Parsing Input: {0}", input.Trim());
            Assert.AreEqual(5, map.Count, "Unexpected number of keys");

            var pairs = map.ToArray();

            Assert.AreEqual(@"website", pairs[0].Key, "Unexpected Key");
            Assert.AreEqual(@"http://en.wikipedia.org/", pairs[0].Value, "Unexpected Value");

            Assert.AreEqual(@"language", pairs[1].Key, "Unexpected Key");
            Assert.AreEqual(@"English", pairs[1].Value, "Unexpected Value");

            Assert.AreEqual(@"message", pairs[2].Key, "Unexpected Key");
            Assert.AreEqual("Welcome to \r\n          Wikipedia!", pairs[2].Value, "Unexpected Value");

            Assert.AreEqual(@"key with spaces", pairs[3].Key, "Unexpected Key");
            Assert.AreEqual(@"This is the value that could be looked up with the key ""key with spaces"".", pairs[3].Value, "Unexpected Value");

            // Note that we don't support unicode (yet)
            Assert.AreEqual(@"tab", pairs[4].Key, "Unexpected Key");
            Assert.AreEqual(@"u0009", pairs[4].Value, "Unexpected Value");
        }
    }
}
