DotNETJPP (.NET Java Properties Parser)
=======================================

Library to support simple parsing of java properties files. Returns a Dictionary<string, string> containing all keys and values.

NOTE: does not support unicode ecapes (yet).

VersionTool (ver.exe)
=====================

This is a command line tool that allows processing of a java .properties file and key value replacement of a target file based on a provided regular expression. This tool also allows some basic levels of value interpolation while replacing values in the target file.

The standard use case for this tool is to read the properties file and replace a source code file's content with values from the properties file, thus giving your source code file static (constant) values for version information. Additionally, your source code file will gain the ability to contain dynamic values produced at the time of a build, again accessible as static (constant) values. To put things more simply, you should be able to access the build time and VCS commit ID directly within the application form a constant field value.

Because this tool uses only paths and regular expression patterns, it is also language agnostic allow it to be used in any build system for any language (providing the system supports executing a shell command).

Property interpolation uses the following format:

        ${DateTime.Property.Property[0].Property...,format string}

Currently only DateTime is supported as property source, but others may be supported in the future. DateTime and the static value property are detected by a regular expression, and the remainder (minus the format string) is processed using Databinder.Eval(). the format string is optional, which means you do not have to include the comma or anything between it and the terminating brace.  Closing braces are not supported within the property expression (not that they would be of any use anyway).

Command interpolation uses the following format:

        $(echo testing)

anything between the () is executed as a shell command. Closing parenthesis are not supported within the command (for now, i'll fix the regex later). This feature is currently non-functional while I focus on testing the property interpolation.

Dictionary interpolation uses the following format:

        $[key]

They key is looked up in the processed .properties file dictionary and replaced with it's value, or an empty string if no such key exists. This feature is currently not available yet, I will stub it in the code in the near future.