﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace DigitalHarmoniX.VersionTool.Test
{
    [TestClass]
    [DeploymentItem(TestDataDirectoryName, TestDataDirectoryName)]
    public class ProgramTest
    {
        private const string TestDataDirectoryName = "TestData";
        private const string VersionFileName = "Version.cs";

        private static FileInfo VersionFile { get { return CreateFile(VersionFileName); } }

        private static string CreateTestDataPath(params string[] paths)
        {
            return Path.Combine(TestDataDirectoryName, Path.Combine(paths));
        }

        private static void ConfirmFileExists(params string[] paths)
        {
            Assert.AreEqual(true, File.Exists(CreateTestDataPath(paths)));
        }

        private static FileInfo CreateFile(params string[] paths)
        {
            return new FileInfo(CreateTestDataPath(paths));
        }

        private static FileInfo CopyVersionFileTo(params string[] paths)
        {
            return VersionFile.CopyTo(Path.Combine(CreateTestDataPath(paths), VersionFile.Name));
        }

        private static void Process(FileInfo propFile, FileInfo versionFile)
        {
            Console.WriteLine("START");
            Console.WriteLine("======================");

            var result = Program.Process(propFile, versionFile, Program.DefaultPattern, Program.DefaultReplacementFormat, Program.DefaultKeyIndex);

            Console.WriteLine("======================");
            Console.WriteLine("END");

            Assert.AreEqual(0, result, "Process was not successful: {0}", result);
        }

        [TestMethod]
        public void SanityTest()
        {
            ConfirmFileExists(VersionFileName);

            ConfirmFileExists("BasicProcessTest", "version.properties");
        }

        [TestMethod]
        public void BasicProcessTest()
        {
            const string TestDirectoryName = "BasicProcessTest";

            var versionFile = CopyVersionFileTo(TestDirectoryName);
            var propFile = CreateFile(TestDirectoryName, "version.properties");

            Process(propFile, versionFile);
        }

        [TestMethod]
        public void BasicGetDateTimePropertyTest()
        {
            var value = DateTime.Now;

            Assert.AreEqual(value.Date.ToString(), Program.GetDateTimeProperty(value,           "Date", null), "Date");
            Assert.AreEqual(value.Day.ToString(), Program.GetDateTimeProperty(value,            "Day", null), "Day");
            Assert.AreEqual(value.DayOfWeek.ToString(), Program.GetDateTimeProperty(value,      "DayOfWeek", null), "DayOfWeek");
            Assert.AreEqual(value.DayOfYear.ToString(), Program.GetDateTimeProperty(value,      "DayOfYear", null), "DayOfYear");
            Assert.AreEqual(value.Hour.ToString(), Program.GetDateTimeProperty(value,           "Hour", null), "Hour");
            Assert.AreEqual(value.Kind.ToString(), Program.GetDateTimeProperty(value,           "Kind", null), "Kind");
            Assert.AreEqual(value.Millisecond.ToString(), Program.GetDateTimeProperty(value,    "Millisecond", null), "Millisecond");
            Assert.AreEqual(value.Minute.ToString(), Program.GetDateTimeProperty(value,         "Minute", null), "Minute");
            Assert.AreEqual(value.Month.ToString(), Program.GetDateTimeProperty(value,          "Month", null), "Month");
            Assert.AreEqual(value.Second.ToString(), Program.GetDateTimeProperty(value,         "Second", null), "Second");
            Assert.AreEqual(value.Ticks.ToString(), Program.GetDateTimeProperty(value,          "Ticks", null), "Ticks");
            Assert.AreEqual(value.TimeOfDay.ToString(), Program.GetDateTimeProperty(value,      "TimeOfDay", null), "TimeOfDay");
            Assert.AreEqual(value.Year.ToString(), Program.GetDateTimeProperty(value,           "Year", null), "Year");
        }

        [TestMethod]
        public void NonExistentPropertyGetDateTimePropertyTest()
        {
            var value = DateTime.Now;

            Assert.AreEqual(value.ToString((string)null), Program.GetDateTimeProperty(value, "DoesNotExist", null), "DoesNotExist");
        }

        [TestMethod]
        public void NullPropertyGetDateTimePropertyTest()
        {
            var value = DateTime.Now;

            Assert.AreEqual(value.ToString((string)null), Program.GetDateTimeProperty(value, null, null), "Null Property");
            Assert.AreEqual(value.ToString((string)null), Program.GetDateTimeProperty(value, string.Empty, null), "Empty String Property");
        }

        [TestMethod]
        public void ValueFormatGetDateTimePropertyTest()
        {
            var value = DateTime.Now;

            Assert.AreEqual(value.ToString("G"), Program.GetDateTimeProperty(value, null, "G"), "G format");
            Assert.AreEqual(value.ToString("R"), Program.GetDateTimeProperty(value, null, "R"), "R format");
            Assert.AreEqual(value.ToString("yyyy-MM-dd"), Program.GetDateTimeProperty(value, null, "yyyy-MM-dd"), "yyyy-MM-dd format");
        }

        [TestMethod]
        public void PropertyFormatGetDateTimePropertyTest()
        {
            var value = DateTime.Now;

            Assert.AreEqual(value.Hour.ToString("D8"), Program.GetDateTimeProperty(value, "Hour", "D8"), "Hour D8 format");
        }

        [TestMethod]
        public void BasicProcessDateTimePropertyTest()
        {
            Assert.AreEqual(DateTime.Now.ToString(), Program.ProcessDateTimeProperty("Now", null), "Now");
            Assert.AreEqual(DateTime.Today.ToString(), Program.ProcessDateTimeProperty("Today", null), "Today");
            Assert.AreEqual(DateTime.UtcNow.ToString(), Program.ProcessDateTimeProperty("UtcNow", null), "UtcNow");

            Assert.AreEqual(DateTime.Now.Hour.ToString(), Program.ProcessDateTimeProperty("Now.Hour", null), "Now.Hour");
            Assert.AreEqual(DateTime.Today.Hour.ToString(), Program.ProcessDateTimeProperty("Today.Hour", null), "Today.Hour");
            Assert.AreEqual(DateTime.UtcNow.Hour.ToString(), Program.ProcessDateTimeProperty("UtcNow.Hour", null), "UtcNow.Hour");

            Assert.AreEqual(DateTime.Now.Hour.ToString("D8"), Program.ProcessDateTimeProperty("Now.Hour", "D8"), "Now.Hour D8 format");
            Assert.AreEqual(DateTime.Today.Hour.ToString("D8"), Program.ProcessDateTimeProperty("Today.Hour", "D8"), "Today.Hour D8 format");
            Assert.AreEqual(DateTime.UtcNow.Hour.ToString("D8"), Program.ProcessDateTimeProperty("UtcNow.Hour", "D8"), "UtcNow.Hour D8 format");
        }

        [TestMethod]
        public void NonExistentPropertyProcessDateTimePropertyTest()
        {
            Assert.AreEqual("Invalid DateTime Property: DoesNotExist", Program.ProcessDateTimeProperty("DoesNotExist", null), "DoesNotExist");
            Assert.AreEqual("No DateTime Property Provided", Program.ProcessDateTimeProperty(string.Empty, null), "Empty string");
        }

        [TestMethod]
        public void BasicDateTimeProcessPropertyTest()
        {
            Assert.AreEqual(DateTime.Now.ToString(), Program.ProcessProperty("DateTime.Now"), "DateTime.Now");
            Assert.AreEqual(DateTime.Today.ToString(), Program.ProcessProperty("DateTime.Today"), "DateTime.Today");
            Assert.AreEqual(DateTime.UtcNow.ToString(), Program.ProcessProperty("DateTime.UtcNow"), "DateTime.UtcNow");

            Assert.AreEqual(DateTime.Now.Hour.ToString(), Program.ProcessProperty("DateTime.Now.Hour"), "DateTime.Now.Hour");
            Assert.AreEqual(DateTime.Today.Hour.ToString(), Program.ProcessProperty("DateTime.Today.Hour"), "DateTime.Today.Hour");
            Assert.AreEqual(DateTime.UtcNow.Hour.ToString(), Program.ProcessProperty("DateTime.UtcNow.Hour"), "DateTime.UtcNow.Hour");

            Assert.AreEqual(DateTime.Now.Hour.ToString("D8"), Program.ProcessProperty("DateTime.Now.Hour,D8"), "DateTime.Now.Hour D8 format");
            Assert.AreEqual(DateTime.Today.Hour.ToString("D8"), Program.ProcessProperty("DateTime.Today.Hour,D8"), "DateTime.Today.Hour D8 format");
            Assert.AreEqual(DateTime.UtcNow.Hour.ToString("D8"), Program.ProcessProperty("DateTime.UtcNow.Hour,D8"), "DateTime.UtcNow.Hour D8 format");
        }

        [TestMethod]
        public void NonExistentPropertyProcessPropertyTest()
        {
            Assert.AreEqual("Invalid Property: DoesNotExist", Program.ProcessProperty("DoesNotExist"), "DoesNotExist");
            Assert.AreEqual("Invalid Property: DoesNotExist.Invalid", Program.ProcessProperty("DoesNotExist.Invalid"), "DoesNotExist.Invalid");
            Assert.AreEqual("Invalid Property: ", Program.ProcessProperty(string.Empty), "Empty string");
        }
    }
}
