﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalHarmoniX.VersionTool.Test.TestData
{
    public static class Version
    {
        public const string VersionSeparator = "";
        public const string SuffixSeparator = "";
        public const string CommitSeparator = "";

        public const string Major = "";
        public const string Minor = "";
        public const string Feature = "";
        public const string Revision = "";

        public const string Suffix = "";

        public const string Commit = "";

        public const string BuildTimestampTicks = "";

        public const string NumericVersion = Major + VersionSeparator + Minor + VersionSeparator + Feature + VersionSeparator + Revision;

        public static string FullVersion
        {
            get 
            {
                var sb = new StringBuilder(NumericVersion);

                if (string.IsNullOrEmpty(Suffix) == false)
                {
                    sb.AppendFormat("{0}{1}", SuffixSeparator, Suffix);
                }

                if (string.IsNullOrEmpty(Commit) == false)
                {
                    sb.AppendFormat("{0}{1}", CommitSeparator, Commit);
                }

                return sb.ToString();
            }
        }

        public static DateTime? BuildTimestamp
        {
            get
            {
                DateTime? timestamp = null;

                try
                {
                    var ticks = Convert.ToInt64(BuildTimestampTicks);

                    timestamp = new DateTime(ticks, DateTimeKind.Utc);
                }
                catch
                {
                }

                return timestamp;
            }
        }

        public static DateTimeOffset? BuildTimestampOffset
        {
            get
            {
                DateTimeOffset? timestampOffset = null;

                var timestamp = BuildTimestamp;

                if (timestamp.HasValue)
                {
                    timestampOffset = new DateTimeOffset(timestamp.Value);
                }

                return timestampOffset;
            }
        }
    }
}
